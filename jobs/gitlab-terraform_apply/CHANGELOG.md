# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-10-07
* Update the docker image used 

## [0.1.0] - 2021-10-01
* Initial version