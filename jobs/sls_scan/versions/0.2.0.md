* Change the image version used for the job to the latest
* Add the option to use `--build`
* Add new variable `SCAN_OPTIONS` for optional arguments