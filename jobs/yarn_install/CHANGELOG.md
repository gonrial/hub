# Changelog
All notable changes to this job will be documented in this file.

## [0.4.1] - 2021-07-25
* Fix cache relative paths
* Add prefix to cache

## [0.4.0] - 2021-05-20
* Add `yarn.lock` as a key to the job's cache
* Remove `cache:key:prefix` to optimize cache 

## [0.3.0] - 2021-04-23
* Upgrade image `node` to `15.14`

## [0.2.0] - 2021-04-20
* Remove keyword `cache:when` for longer backward compatibility

## [0.1.0] - 2021-02-11
* Initial version
