# Changelog
All notable changes to this job will be documented in this file.

## [0.3.0] - 2021-01-29
* Update default output location from `documentation_build/` to `website_build/`
* Update `rules` section to enable the job on `$CI_DEFAULT_BRANCH` branch

## [0.2.0] - 2020-11-30
* Fix files aggregation for folder `public` in some case instead of creating the folder with the documentation

## [0.1.0] - 2020-10-02
* Initial version
