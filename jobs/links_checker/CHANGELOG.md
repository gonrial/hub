# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-02-17
* Fix `LICHE_EXCLUDE` feature

## [0.1.0] - 2020-12-10
* Initial version