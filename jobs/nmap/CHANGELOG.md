# Changelog
All notable changes to this job will be documented in this file.

## [0.2.3] - 2021-10-07
* Upgrade `libxslt` default version
* Upgrade `instrumentisto/nmap` version to `7.92`

## [0.2.2] - 2021-03-04
* Enable `artifact:expose_as` option to display job result in merge request

## [0.2.1] - 2021-03-03
* Fix version of `libxslt` package to `1.1.34-r0` through variable `LIBXSLT_VERSION`

## [0.1.0] - 2020-12-22
* Initial version